 <!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
	</head>
	<body>
		<?php

		$n = 1;

		function calculoColor(){
			$color = "rgb(" . rand(0, 255) . ", " . rand(0,255) . ", " . rand(0,255) . ")";
			return $color;
		}

		function dibujarCirculo($x, $y, $n){
                        echo "N: " . $n;
			echo '<circle cx="' . $x . '" cy="' . $y . '" r="50" fill = "' . calculoColor() . '" id="circulo' . $n . '" onclick="ocultar(' . $n . ')" />';
		}

		?>
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="1000px" height="1000px" style="display:block;margin:0px auto;">
		<?php
		dibujarCirculo(50, 50, $n);
		$n++;
		dibujarCirculo(100, 50, $n);
		$n++;
		dibujarCirculo(200, 250, $n);
		?>
		<script>
			function ocultar(number){
				var num = number;

				document.getElementById("circulo" + num).style.display = "none";
			}
		</script>
	</body>
</html>