<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<script type="text/javascript">
			window.addEventListener("load", arranca);
			function arranca(){
				document.querySelector("line").addEventListener("click", salta);
			}

			function salta(event){
				var valor;
				valor = Math.random() * (100 - 1) + 1;
				event.target.setAttribute("x2", valor);
			}
		</script>
	</head>
	<body>
		<?php

		function calculoColor(){
			$color = "rgb(" . rand(0, 255) . ", " . rand(0,255) . ", " . rand(0,255) . ")";
			return $color;
		}

		$color = calculoColor();
		?>
		<p>Color: <?= $color ?></p>
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" style="display:block;margin:0px auto;">
		<circle cx="50" cy="50" r="50" fill="<?= $color ?>" />
		</svg>
	</body>
</html>